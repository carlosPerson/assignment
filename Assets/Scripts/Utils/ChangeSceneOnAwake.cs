﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSceneOnAwake : MonoBehaviour {

    void Awake()
    {
        SRScenes.Game.Load();
    }
}
