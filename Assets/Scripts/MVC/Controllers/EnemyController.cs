﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    Enemy enemy;

    public void Init(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public void SetPosition(Tile spawnTile)
    {
        enemy.gridPosition = spawnTile.gridPosition;
    }

    public void ChangeState(StateMachine.StateType newStateType)
    {
        enemy.ChangeState(newStateType);
    }

}
