﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    Turret turret;
    UITurretElement uiTurret;

    public Turret Turret
    {
        get { return turret; }
    }

    public UITurretElement UITurret
    {
        get { return uiTurret; }
    }

    public void Init(Turret turret, UITurretElement uiTurret)
    {
        this.turret = turret;
        this.uiTurret = uiTurret;
        EventManager.AddListener<TurretRotatedEvent>(OnTurretRotatedEvent);
        EventManager.AddListener<TurretShotEvent>(OnTurretShotEvent);
        EventManager.AddListener<TurretDestroyedEvent>(OnTurretDestroyedEvent);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<TurretRotatedEvent>(OnTurretRotatedEvent);
        EventManager.RemoveListener<TurretShotEvent>(OnTurretShotEvent);
        EventManager.RemoveListener<TurretDestroyedEvent>(OnTurretDestroyedEvent);
    }

    void OnTurretRotatedEvent(TurretRotatedEvent e)
    {
        if (turret.Tile.row == e.tileRow && turret.Tile.column == e.tileColumn)
        {
            uiTurret.SetRotation(e.targetRotation);
        }
    }

    void OnTurretShotEvent(TurretShotEvent e)
    {
        if (turret.Tile.row == e.tileRow && turret.Tile.column == e.tileColumn)
        {
            uiTurret.SetShooting();
        }
    }

    void OnTurretDestroyedEvent(TurretDestroyedEvent e)
    {
        if (turret.Tile.row == e.tileRow && turret.Tile.column == e.tileColumn)
        {
            turret.Destroy();
            Destroy(uiTurret.gameObject);
        }
    }
}
