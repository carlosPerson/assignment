﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileController : MonoBehaviour
{
    [SerializeField]
    Button button;
    Tile tile; // the tile this controller manages
    UITileElement uiTile;
    UIMap uiMap;
    GameObject turretPrefab;

    public Tile MyTile
    {
        get { return tile; }
    }

    public void Init(Tile tile, UITileElement uiTile)
    {
        this.tile = tile;
        this.uiTile = uiTile;
        uiMap = GUIManager.Instance.GetGUI(GUIType.MAP) as UIMap;
        turretPrefab = SRResources.GUI.Map.TurretElement.Load();
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(OnTileClicked);

        EventManager.AddListener<OpeningTileContextMenuEvent>(OnOpeningTileContextMenuEvent);
        EventManager.AddListener<GameOverEvent>(OnGameOverEvent);
        EventManager.AddListener<TileOptionPurchasedEvent>(OnTileOptionPurchasedEvent);
        EventManager.AddListener<TilesBlockingButtonClickedEvent>(OnTilesBlockingButtonClickedEvent);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<OpeningTileContextMenuEvent>(OnOpeningTileContextMenuEvent);
        EventManager.RemoveListener<GameOverEvent>(OnGameOverEvent);
        EventManager.RemoveListener<TileOptionPurchasedEvent>(OnTileOptionPurchasedEvent);
        EventManager.RemoveListener<TilesBlockingButtonClickedEvent>(OnTilesBlockingButtonClickedEvent);
    }

    void OnTileClicked()
    {
        if (tile.CanBuildTurret() && !tile.CheckIfOccupied() && tile.GetAvailableTileOptions().Count > 0)
        {
            OpenContextMenu();
        }
    }

    void OpenContextMenu()
    {
        EventManager.TriggerEvent(new OpeningTileContextMenuEvent());
        uiTile.OpenContextMenu(tile.GetAvailableTileOptions(), OnClickedOptionCallback);
        uiMap.tilesBlockingButton.gameObject.SetActive(true);
        TimeManager.Instance.Pause();
    }

    void OnClickedOptionCallback(Tile.TileOptionType clickedOption)
    {
        bool success = StoreManager.Instance.PurchaseOption(clickedOption);
        if (success)
        {
            // TODO play audio successful purchase
            EventManager.TriggerEvent(new TileOptionPurchasedEvent(tile.row, tile.column, clickedOption));
            uiMap.tilesBlockingButton.gameObject.SetActive(false);
            TimeManager.Instance.Resume();
            CreateTurret(clickedOption);
        }
        else
        {
            // TODO play audio failed purchase
        }
    }

    void CreateTurret(Tile.TileOptionType clickedOption)
    {
        Turret turret = new Turret(StoreManager.Instance.GetTurretDefinition(clickedOption), tile);
        tile.SetTurret(turret);
        GameObject turretInstance = Instantiate(turretPrefab, uiMap.uiTiles[tile.row][tile.column].thisRect, false);
        UITurretElement uiTurret = turretInstance.GetComponent<UITurretElement>();
        uiTurret.Init(turret);
        TurretController turretController = turretInstance.GetComponent<TurretController>();
        turretController.Init(turret, uiTurret);
        turret.Init();
        EventManager.TriggerEvent(new TurretCreatedEvent(tile.row, tile.column));
    }

    void OnOpeningTileContextMenuEvent(OpeningTileContextMenuEvent e)
    {
        uiTile.CloseContextMenu();
    }

    void OnGameOverEvent(GameOverEvent e)
    {
        uiTile.CloseContextMenu();
    }

    void OnTileOptionPurchasedEvent(TileOptionPurchasedEvent e)
    {
        uiTile.CloseContextMenu();
    }

    void OnTilesBlockingButtonClickedEvent(TilesBlockingButtonClickedEvent e)
    {
        uiTile.CloseContextMenu();
    }
}
