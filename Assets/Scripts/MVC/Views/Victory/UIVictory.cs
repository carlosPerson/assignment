﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVictory : UIScreen
{
    [SerializeField]
    public Button replayButton;

    protected override void Open()
    {
        base.Open();
        replayButton.onClick.RemoveAllListeners();
        replayButton.onClick.AddListener(OnClickReplayButton);
    }

    void OnClickReplayButton()
    {
        SRScenes.Loading.Load();
    }
}
