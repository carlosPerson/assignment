﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEnemy : MonoBehaviour
{
    public Image enemyImage;
    public RectTransform thisRect;
    [SerializeField]
    Slider healthBarSlider;
    [SerializeField]
    GameObject shotImage;
    [SerializeField]
    float shootingTotalTime;
    Enemy enemy;
    UIMap uiMap;
    bool isBeingShot;
    float shootingElapsedTime;

    public void Init(Enemy enemy)
    {
        this.enemy = enemy;
        enemyImage.sprite = enemy.EnemySprite;
        healthBarSlider.maxValue = enemy.MaxHP;
        healthBarSlider.value = enemy.MaxHP;
        uiMap = GUIManager.Instance.GetGUI(GUIType.MAP) as UIMap;
        shotImage.SetActive(false);
        EventManager.AddListener<EnemyMovedEvent>(OnEnemyMovedEvent);
        EventManager.AddListener<EnemyReceivedDamageEvent>(OnEnemyReceivedDamageEvent);
        EventManager.AddListener<EnemyDiedEvent>(OnEnemyDiedEvent);
        EventManager.AddListener<UpdateEvent>(OnUpdateEvent);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<EnemyMovedEvent>(OnEnemyMovedEvent);
        EventManager.RemoveListener<EnemyReceivedDamageEvent>(OnEnemyReceivedDamageEvent);
        EventManager.RemoveListener<EnemyDiedEvent>(OnEnemyDiedEvent);
        EventManager.RemoveListener<UpdateEvent>(OnUpdateEvent);
    }

    void OnEnemyMovedEvent(EnemyMovedEvent e)
    {
        if (e.enemyId == enemy.id)
        {
            UpdatePosition();
        }
    }

    void UpdatePosition()
    {
        float finalXPosition = uiMap.uiTiles[enemy.CurrentTile.row][enemy.CurrentTile.column].thisRect.anchoredPosition.x;
        float finalYPosition = uiMap.uiTiles[enemy.CurrentTile.row][enemy.CurrentTile.column].thisRect.anchoredPosition.y;
        Vector2 origin = uiMap.uiTiles[enemy.CurrentTile.row][enemy.CurrentTile.column].thisRect.anchoredPosition;
        Vector2 destination = uiMap.uiTiles[enemy.NextTile.row][enemy.NextTile.column].thisRect.anchoredPosition;
        float rowFraction = enemy.gridPosition.x % 1;
        float columnFraction = enemy.gridPosition.y % 1;
        bool isMovingHorizontallyBackwards = enemy.CurrentTile.column > enemy.NextTile.column;
        bool isMovingVerticallyBackwards = enemy.CurrentTile.row > enemy.NextTile.row;
        if (isMovingHorizontallyBackwards && columnFraction != 0f) columnFraction = 1 - columnFraction;
        if (isMovingVerticallyBackwards && rowFraction != 0f) rowFraction = 1 - rowFraction;
        if (rowFraction > 0.0f)
        {
            finalYPosition = Vector2.Lerp(origin, destination, rowFraction).y;
        }
        if (columnFraction > 0.0f)
        {
            finalXPosition = Vector2.Lerp(origin, destination, columnFraction).x;
        }
        thisRect.anchoredPosition = new Vector2(finalXPosition, finalYPosition);
    }

    void OnEnemyReceivedDamageEvent(EnemyReceivedDamageEvent e)
    {
        if (enemy.id == e.enemyId)
        {
            shotImage.SetActive(true);
            healthBarSlider.value = enemy.HP;
            SetBeingShot();
        }
    }

    void OnEnemyDiedEvent(EnemyDiedEvent e)
    {
        if (enemy.id == e.enemyId)
        {
            Destroy(gameObject);
        }
    }

    public void SetBeingShot()
    {
        isBeingShot = enabled;
        shotImage.SetActive(true);
        shootingElapsedTime = 0;
    }

    void OnUpdateEvent(UpdateEvent e)
    {
        if (isBeingShot)
        {
            if (shootingElapsedTime >= shootingTotalTime)
            {
                shotImage.SetActive(false);
                isBeingShot = false;
                return;
            }
            shootingElapsedTime += Time.deltaTime;
        }
    }
}
