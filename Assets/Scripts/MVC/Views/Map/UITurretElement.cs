﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITurretElement : MonoBehaviour
{
    [SerializeField]
    Image turretImage;
    [SerializeField]
    GameObject shotImage;
    [SerializeField]
    float shootingTotalTime;
    bool isShooting;
    float shootingElapsedTime;

    public void Init(Turret turret)
    {
        turretImage.sprite = turret.Sprite;
        shotImage.SetActive(false);
        EventManager.AddListener<UpdateEvent>(OnUpdateEvent);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<UpdateEvent>(OnUpdateEvent);
    }

    public void SetRotation(Quaternion targetRotation)
    {
        transform.rotation = targetRotation;
    }

    public void SetShooting()
    {
        isShooting = enabled;
        shotImage.SetActive(true);
        shootingElapsedTime = 0;
    }

    void OnUpdateEvent(UpdateEvent e)
    {
        if (isShooting)
        {
            if (shootingElapsedTime >= shootingTotalTime)
            {
                shotImage.SetActive(false);
                isShooting = false;
                return;
            }
            shootingElapsedTime += Time.deltaTime;
        }
    }
}
