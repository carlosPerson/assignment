﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITileContextMenuElement : MonoBehaviour
{
    public Image elementImage;
    public Text priceText;
    public Button purchaseButton;
}
