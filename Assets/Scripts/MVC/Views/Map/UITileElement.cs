﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class UITileElement : MonoBehaviour
{
    public Image tileImage;
    public RectTransform thisRect;
    public RadialLayout radialLayout;
    public float radialLayoutOpeningTime = 0.5f;
    public float angleMaxValue = 360;
    float radialLayoutOpeningTimeElapsedTime;
    Coroutine openContextMenuCoroutine;
    GameObject contextMenuElementPrefab;
    RectTransform radialLayoutRectTransform;

    public void Init(Tile tile)
    {
        tileImage.sprite = tile.TileSprite;
        contextMenuElementPrefab = SRResources.GUI.Map.TileContextMenu.TileContextMenuElement.Load();
        radialLayoutRectTransform = radialLayout.GetComponent<RectTransform>();
    }

    public void OpenContextMenu(List<Tile.TileOptionType> optionsList, Action<Tile.TileOptionType> onClickedOptionCallback)
    {
        InstantiateOptions(optionsList, onClickedOptionCallback);
        radialLayout.MaxAngle = 0;
        radialLayoutOpeningTimeElapsedTime = 0;
        openContextMenuCoroutine = StartCoroutine(OpenContextMenuCoroutine());
    }

    void InstantiateOptions(List<Tile.TileOptionType> optionsList, Action<Tile.TileOptionType> onClickedOptionCallback)
    {
        foreach (Tile.TileOptionType tileOption in optionsList)
        {
            UITileContextMenuElement uiContextMenuElement = Instantiate(contextMenuElementPrefab, radialLayout.transform, false).GetComponent<UITileContextMenuElement>();
            uiContextMenuElement.purchaseButton.onClick.RemoveAllListeners();
            uiContextMenuElement.purchaseButton.interactable = StoreManager.Instance.CanPurchaseOption(tileOption);
            switch (tileOption)
            {
                case Tile.TileOptionType.TURRET:
                    uiContextMenuElement.elementImage.sprite = SRResources.GameBalance.TurretDefinitions.Turret.Load().sprite;
                    uiContextMenuElement.priceText.text = SRResources.GameBalance.TurretDefinitions.Turret.Load().price.ToString();
                    uiContextMenuElement.purchaseButton.onClick.AddListener(
                        () =>
                        {
                            onClickedOptionCallback.Invoke(Tile.TileOptionType.TURRET);
                        });
                    break;
                case Tile.TileOptionType.FAST_TURRET:
                    uiContextMenuElement.elementImage.sprite = SRResources.GameBalance.TurretDefinitions.TurretFast.Load().sprite;
                    uiContextMenuElement.priceText.text = SRResources.GameBalance.TurretDefinitions.TurretFast.Load().price.ToString();
                    uiContextMenuElement.purchaseButton.onClick.AddListener(
                        () =>
                        {
                            onClickedOptionCallback.Invoke(Tile.TileOptionType.FAST_TURRET);
                        });
                    break;
                //case Tile.TileOptionType.UPGRADE:
                //    break;
                default:
                    Debug.LogError("Unknown tile option type: " + tileOption.ToString());
                    break;
            }
        }
    }

    IEnumerator OpenContextMenuCoroutine()
    {
        radialLayout.gameObject.SetActive(true);
        radialLayout.MaxAngle = 0;
        while (radialLayoutOpeningTimeElapsedTime < radialLayoutOpeningTime)
        {
            radialLayout.MaxAngle = (radialLayoutOpeningTimeElapsedTime / radialLayoutOpeningTime) * angleMaxValue;
            LayoutRebuilder.ForceRebuildLayoutImmediate(radialLayoutRectTransform); // Unity layouts don't update every frame
            yield return null;
            radialLayoutOpeningTimeElapsedTime += Time.deltaTime;
        }
        // snap
        radialLayout.MaxAngle = angleMaxValue;
        LayoutRebuilder.ForceRebuildLayoutImmediate(radialLayoutRectTransform); // Unity layouts don't update every frame
    }

    public void CloseContextMenu()
    {
        if (openContextMenuCoroutine != null) StopCoroutine(openContextMenuCoroutine);
        foreach (Transform child in radialLayout.transform) Destroy(child.gameObject);
        radialLayout.gameObject.SetActive(false);
    }
}
