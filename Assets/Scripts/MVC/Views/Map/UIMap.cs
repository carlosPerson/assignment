﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMap : UIScreen
{
    public Transform tilesContainer, enemiesContainer;
    [SerializeField]
    GridLayoutGroup gridLayout;
    public List<List<UITileElement>> uiTiles;
    public Button tilesBlockingButton;
    int rowCounter;
    int columnCount;

    public void Setup(int rowCount, int columnCount)
    {
        // Set row divisions
        gridLayout.constraint = UnityEngine.UI.GridLayoutGroup.Constraint.FixedRowCount;
        gridLayout.constraintCount = rowCount;
        this.columnCount = columnCount;
        uiTiles = new List<List<UITileElement>>();
        uiTiles.Add(new List<UITileElement>());
        rowCounter = 0;

        tilesBlockingButton.gameObject.SetActive(false);
        tilesBlockingButton.onClick.RemoveAllListeners();
        tilesBlockingButton.onClick.AddListener(OnTilesBlockingButtonClicked);
    }

    public void AddTile(UITileElement uiTileElement)
    {
        if (uiTiles[rowCounter].Count >= columnCount)
        {
            uiTiles.Add(new List<UITileElement>()); // Add new row when limit reached
            rowCounter++;
        }
        uiTiles[rowCounter].Add(uiTileElement);
    }

    void OnTilesBlockingButtonClicked()
    {
        EventManager.TriggerEvent(new TilesBlockingButtonClickedEvent());
        tilesBlockingButton.gameObject.SetActive(false);
        TimeManager.Instance.Resume();
    }
}
