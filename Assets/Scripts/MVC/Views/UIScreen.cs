﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIScreen : MonoBehaviour
{
    [HideInInspector]
    public Canvas parentCanvas;
    public int sortingOrder;

    void Awake()
    {
        Open();
    }

    protected virtual void Open()
    {
    }

    public virtual void Close()
    {
        Destroy(gameObject);
    }
}
