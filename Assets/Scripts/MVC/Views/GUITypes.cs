﻿public enum GUIType
{
    MAP,
    HUD,
    MAIN_MENU,
    GAME_OVER,
    VICTORY
}
