﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHUD : UIScreen
{
    [SerializeField]
    Text currencyText;

    protected override void Open()
    {
        base.Open();
        UpdateCurrencyText();
        EventManager.AddListener<CurrencyChangedEvent>(OnCurrencyChangedEvent);
    }

    public override void Close()
    {
        base.Close();
        EventManager.RemoveListener<CurrencyChangedEvent>(OnCurrencyChangedEvent);
    }

    void OnCurrencyChangedEvent(CurrencyChangedEvent e)
    {
        UpdateCurrencyText();
    }

    void UpdateCurrencyText()
    {
        currencyText.text = StoreManager.Instance.Currency.ToString();
    }
}
