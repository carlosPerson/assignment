﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Game Balance file", menuName = "TowerDefense/Create GameBalance")]
public class GameBalanceDefinition : ScriptableObject
{
    [Header("Timings")]
    public float timeBetweenRoundsInSeconds;
    public float timeBetweenEnemySpawnInSeconds;

    [Header("Waves")]
    public List<WaveData> waves;

    [Header("Currency")]
    public int initialCurrency;
    public int currencyPerEnemyKilled;

    [Header("Settings")]
    public float shootingAngleBoundsInDegrees;
}
