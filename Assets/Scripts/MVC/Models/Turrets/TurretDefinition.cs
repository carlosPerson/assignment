﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Turret", menuName = "TowerDefense/Create Turret")]
public class TurretDefinition : ScriptableObject
{
    public int damage;
    public float fireRateInSeconds;
    public float range;
    public float rotationSpeed;
    public int price;
    public Sprite sprite;
}
