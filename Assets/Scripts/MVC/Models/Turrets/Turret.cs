﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret
{
    int damage;
    float fireRateInSeconds;
    float range;
    float rotationSpeed;
    int price;
    Sprite sprite;
    Tile tile;
    float cooldown;
    Quaternion currentRotation, targetRotation;

    public int Damage
    {
        get { return damage; }
    }

    public float FireRateInSeconds
    {
        get { return fireRateInSeconds; }
    }

    public float Range
    {
        get { return range; }
    }

    public float RotationSpeed
    {
        get { return rotationSpeed; }
    }

    public int Price
    {
        get { return price; }
    }

    public Sprite Sprite
    {
        get { return sprite; }
    }

    public Tile Tile
    {
        get { return tile; }
    }

    public Turret(TurretDefinition turretDefinition, Tile tile)
    {
        damage = turretDefinition.damage;
        fireRateInSeconds = turretDefinition.fireRateInSeconds;
        range = turretDefinition.range;
        rotationSpeed = turretDefinition.rotationSpeed;
        currentRotation = Quaternion.identity;
        targetRotation = Quaternion.identity;
        price = turretDefinition.price;
        sprite = turretDefinition.sprite;
        this.tile = tile;
    }

    public void Init()
    {
        EventManager.AddListener<UpdateEvent>(OnUpdateEvent);
    }

    public void Destroy()
    {
        EventManager.RemoveListener<UpdateEvent>(OnUpdateEvent);
    }

    void OnUpdateEvent(UpdateEvent e)
    {
        UpdateCooldown();
        Enemy closestEnemy = FindClosestEnemy();
        if (closestEnemy != null)
        {
            RotateToEnemy(closestEnemy);
            if (IsEnemyInRange(closestEnemy) && IsBulletReady() && IsAngleInBounds(closestEnemy))
            {
                Fire(closestEnemy);
            }
        }
    }

    void UpdateCooldown()
    {
        cooldown -= Time.deltaTime;
        if (cooldown <= 0) cooldown = 0;
    }

    Enemy FindClosestEnemy()
    {
        float minDistance = Mathf.Infinity;
        Enemy closestEnemy = null;
        foreach (Enemy enemy in SpawnerManager.Instance.SpawnedEnemiesList)
        {
            float enemyDistance = Vector2.Distance(enemy.gridPosition, tile.gridPosition);
            if (enemyDistance < minDistance)
            {
                minDistance = enemyDistance;
                closestEnemy = enemy;
            }
        }
        return closestEnemy;
    }

    void RotateToEnemy(Enemy closestEnemy)
    {
        float angleToTarget = CalculateAngleToTarget(closestEnemy);
        targetRotation = Quaternion.AngleAxis(angleToTarget, Vector3.forward);
        currentRotation = Quaternion.Slerp(currentRotation, targetRotation, Time.deltaTime * RotationSpeed);
        EventManager.TriggerEvent(new TurretRotatedEvent(tile.row, tile.column, currentRotation));
    }

    float CalculateAngleToTarget(Enemy closestEnemy)
    {
        Vector3 targetVector = closestEnemy.gridPosition - tile.gridPosition;
        return Mathf.Atan2(targetVector.y, targetVector.x) * Mathf.Rad2Deg;
    }

    bool IsEnemyInRange(Enemy closestEnemy)
    {
        return Vector2.Distance(closestEnemy.gridPosition, tile.gridPosition) <= range;
    }

    bool IsBulletReady()
    {
        return cooldown <= 0;
    }

    bool IsAngleInBounds(Enemy closestEnemy)
    {
        return currentRotation.Approximately(
            targetRotation, 
            SRResources.GameBalance.General.GameBalance.Load().shootingAngleBoundsInDegrees / 360f);
    }

    void Fire(Enemy closestEnemy)
    {
        closestEnemy.ReceiveDamage(damage);
        cooldown = fireRateInSeconds;
        EventManager.TriggerEvent(new TurretShotEvent(tile.row, tile.column));
    }
}
