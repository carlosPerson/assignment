﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "TowerDefense/Create Enemy")]
public class EnemyDefinition : ScriptableObject
{
    public int maxHP;
    public float tileTravelTime;
    public Sprite enemySprite;
}
