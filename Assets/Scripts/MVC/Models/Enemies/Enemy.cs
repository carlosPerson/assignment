﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Enemy
{
    public readonly int id;
    StateMachine stateMachine;
    int hp, maxHP;
    float tileTravelTime;
    Sprite enemySprite;

    public Vector2 gridPosition;
    List<Tile> path;
    int pathIndex;
    bool canDestroyTurrets;

    public int HP
    {
        get { return hp; }
    }
    public int MaxHP
    {
        get { return maxHP; }
    }
    public float TileTravelTime
    {
        get { return tileTravelTime; }
    }
    public Sprite EnemySprite
    {
        get { return enemySprite; }
    }

    public Tile CurrentTile
    {
        get { return path[pathIndex]; }
    }

    public Tile NextTile
    {
        get
        {
            if (pathIndex + 1 < path.Count)
            {
                return path[pathIndex + 1];
            }
            else return CurrentTile;
        }
    }

    public Enemy(int id, EnemyDefinition enemyDefinition, Tile spawnTile, Tile playerBaseTile)
    {
        this.id = id;
        maxHP = enemyDefinition.maxHP;
        hp = maxHP;
        tileTravelTime = enemyDefinition.tileTravelTime;
        enemySprite = enemyDefinition.enemySprite;
        pathIndex = 0;
        canDestroyTurrets = false;
        UpdatePathFinding(spawnTile, playerBaseTile);
        gridPosition = spawnTile.gridPosition;
        stateMachine = new StateMachine(this);
    }

    public void UpdatePathFinding(Tile currentTile, Tile playerBaseTile, Tile nextTile = null)
    {
        Tile startingTile = currentTile;
        if (nextTile != null) startingTile = nextTile;
        List<Node> nodePath = AStar.FindPath
        (
            MapManager.Instance.Grid2DWithTurrets.GetNodeFromPosition(startingTile.column, startingTile.row),
            MapManager.Instance.Grid2DWithTurrets.GetNodeFromPosition(playerBaseTile.column, playerBaseTile.row),
            MapManager.Instance.Grid2DWithTurrets
        );
        if (nodePath != null)
        {
            canDestroyTurrets = false;
            CreateTilePath(currentTile, nextTile, nodePath);
        }
        else
        {
            // all paths blocked by turrets, or impossible maze
            nodePath = AStar.FindPath
            (
                MapManager.Instance.Grid2DNoTurrets.GetNodeFromPosition(startingTile.column, startingTile.row),
                MapManager.Instance.Grid2DNoTurrets.GetNodeFromPosition(playerBaseTile.column, playerBaseTile.row),
                MapManager.Instance.Grid2DNoTurrets
            );
            if (nodePath != null)
            {
                canDestroyTurrets = true;
                CreateTilePath(currentTile, NextTile, nodePath);
            }
            else
            {
                Debug.LogError("Impossible maze found. Cannot go from origin to destination even without turrets.");
                return;
            }
        }
    }

    void CreateTilePath(Tile currentTile, Tile nextTile, List<Node> nodePath)
    {
        path = new List<Tile>();
        path.Add(currentTile);
        if (nextTile != null) path.Add(nextTile);
        foreach (Node node in nodePath)
        {
            path.Add(MapManager.Instance.map[node.Y][node.X]);
        }
        pathIndex = 0;
    }

    public bool IsTileInPath(int tileRow, int tileColumn)
    {
        foreach (Tile tile in path)
        {
            if (tile.row == tileRow && tile.column == tileColumn) return true;
        }
        return false;
    }

    public void ChangeState(StateMachine.StateType newStateType)
    {
        stateMachine.ChangeState(newStateType);
    }

    public void TargetTileReached()
    {
        if (pathIndex + 1 < path.Count) pathIndex++;
        if (path[pathIndex] is TilePlayerBase) EventManager.TriggerEvent(new GameOverEvent());
        else if (path[pathIndex].HasTurret() && canDestroyTurrets)
        {
            path[pathIndex].DestroyTurret();
        }
    }

    public void ReceiveDamage(int damage)
    {
        hp -= damage;
        if (hp < 0) hp = 0;
        EventManager.TriggerEvent(new EnemyReceivedDamageEvent(id));
        if (hp == 0)
        {
            Die();
            EventManager.TriggerEvent(new EnemyDiedEvent(id));
        }
    }

    void Die()
    {
        stateMachine.CurrentState.Exit();
    }
}
