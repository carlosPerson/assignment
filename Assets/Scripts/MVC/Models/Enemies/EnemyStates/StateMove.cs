﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMove : State
{
    float thisTileElapsedTime, thisTileTravelTime;
    EnemyMovedEvent enemyMovedEvent;

    public StateMove(Enemy enemy) : base(enemy)
    {
    }

    public override void Enter()
    {
        base.Enter();
        thisTileElapsedTime = 0;
        CalculateTravelTime();
        enemyMovedEvent = new EnemyMovedEvent(enemy.id);
        EventManager.AddListener<UpdateEvent>(OnUpdateEvent);
    }

    void OnUpdateEvent(UpdateEvent e)
    {
        Execute();
    }

    public override void Execute()
    {
        base.Execute();
        if (enemy.NextTile == null) return;

        enemy.gridPosition = Vector2.Lerp(enemy.CurrentTile.gridPosition, enemy.NextTile.gridPosition, thisTileElapsedTime / thisTileTravelTime);
        if (thisTileElapsedTime >= thisTileTravelTime)
        {
            // target reached
            thisTileElapsedTime = 0;
            enemy.gridPosition = enemy.NextTile.gridPosition; // snap for precision
            enemy.TargetTileReached();
            CalculateTravelTime();
        }
        else
        {
            thisTileElapsedTime += Time.deltaTime;
        }
        EventManager.TriggerEvent(enemyMovedEvent);
    }

    void CalculateTravelTime()
    {
        thisTileTravelTime = enemy.TileTravelTime;
        bool isDiagonalMovement =
            enemy.CurrentTile.gridPosition.x != enemy.NextTile.gridPosition.x &&
            enemy.CurrentTile.gridPosition.y != enemy.NextTile.gridPosition.y;

        if (isDiagonalMovement) thisTileTravelTime *= Mathf.Sqrt(2); // diagonal movements cost more, pythagoras
    }

    public override void Exit()
    {
        base.Exit();
        // recycle stuff
        EventManager.RemoveListener<UpdateEvent>(OnUpdateEvent);
    }
}
