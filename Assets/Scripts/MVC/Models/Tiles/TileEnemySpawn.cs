﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileEnemySpawn : Tile
{
    public TileEnemySpawn(int row, int column) : base(SRResources.GameBalance.TileDefinitions.TileEnemySpawnDefinition.Load(), row, column)
    {
    }
}
