﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileFloor : Tile
{
    public TileFloor(int row, int column) : base(SRResources.GameBalance.TileDefinitions.TileFloorDefinition.Load(), row, column)
    {
    }

    public override List<TileOptionType> GetAvailableTileOptions()
    {
        List<TileOptionType> options = new List<TileOptionType>();
        if (HasTurret())
        {
            // TODO destroy turret, upgrade, etc.
        }
        else
        {
            options.Add(TileOptionType.TURRET);
            options.Add(TileOptionType.FAST_TURRET);
        }
        return options;
    }
}
