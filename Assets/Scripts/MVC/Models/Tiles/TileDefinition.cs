﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tile", menuName = "TowerDefense/Create Tile")]
public class TileDefinition : ScriptableObject
{
    public bool isPassable;
    public Sprite sprite;
    public int cost;
}
