﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileWall : Tile
{
    public TileWall(int row, int column) : base(SRResources.GameBalance.TileDefinitions.TileWallDefinition.Load(), row, column)
    {
    }
}
