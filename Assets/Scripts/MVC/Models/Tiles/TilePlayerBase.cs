﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePlayerBase : Tile
{
    public TilePlayerBase(int row, int column) : base(SRResources.GameBalance.TileDefinitions.TilePlayerBaseDefinition.Load(), row, column)
    {
    }
}
