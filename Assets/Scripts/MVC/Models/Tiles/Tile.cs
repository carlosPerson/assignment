﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tile
{
    protected bool isPassable;
    Sprite tileSprite;
    public readonly int row, column;
    public readonly Vector2 gridPosition;
    private int cost;
    Turret turret;

    public Sprite TileSprite
    {
        get { return tileSprite; }
    }

    public Turret Turret
    {
        get { return turret; }
    }

    public int Cost
    {
        get { return cost; }
    }

    public enum TileOptionType
    {
        TURRET, FAST_TURRET
    }

    public Tile(TileDefinition tileDefinition, int row, int column)
    {
        isPassable = tileDefinition.isPassable;
        tileSprite = tileDefinition.sprite;
        cost = tileDefinition.cost;
        this.row = row;
        this.column = column;
        gridPosition = new Vector2(row, column);
    }

    public virtual bool CanPassThrough()
    {
        return isPassable && !HasTurret();
    }

    public bool CheckIfOccupied()
    {
        if (HasTurret()) return true;
        foreach (Enemy enemy in SpawnerManager.Instance.SpawnedEnemiesList)
        {
            if (enemy.CurrentTile == this || enemy.NextTile == this)
            {
                return true;
            }
        }
        return false;
    }

    public bool CanBuildTurret()
    {
        return this is TileFloor;
    }

    public virtual List<TileOptionType> GetAvailableTileOptions()
    {
        return new List<TileOptionType>(); // no options by default
    }

    public bool HasTurret()
    {
        return turret != null;
    }

    public void SetTurret(Turret turret)
    {
        this.turret = turret;
    }

    public void DestroyTurret()
    {
        this.turret = null;
        EventManager.TriggerEvent(new TurretDestroyedEvent(row, column));
    }
}
