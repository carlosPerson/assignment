﻿using System;
using UnityEngine;

[System.Serializable]
public class Node
{
    [SerializeField] private int xPosition;
    [SerializeField] private int yPosition;
        
    public Node(int x, int y, int cost, bool isWalkable)
    {
        xPosition = x;
        yPosition = y;
        Walkable = isWalkable;
        Cost = cost;
    }

    public int X
    {
        get { return xPosition; }
    }

    public int Y
    {
        get { return yPosition; }
    }

    private bool walkable;

    public bool Walkable
    {
        get { return walkable; }
        set { walkable = value; }
    }

    public int Cost
    {
        get;
        set;
    }

    public void UpdateValues(int newCost, bool newWalkable)
    {
        Cost = newCost;
        walkable = newWalkable;
    }

    #region AStar specific
    private Node parent;

    public Node Parent
    {
        get { return parent; }
        set { parent = value; }
    }
        
    public int GCost { get; set; }
    public int HCost { get; set; }

    public int FCost { get { return GCost + HCost; } }
    #endregion
}
