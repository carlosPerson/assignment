﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class MapManager : Singleton<MapManager>
{
    public List<List<Tile>> map;
    List<TileController> tileControllers;
    Grid2D grid2DWithTurrets;
    Grid2D grid2DNoTurrets;

    public List<TileController> TileControllers
    {
        get { return tileControllers; }
    }

    public Grid2D Grid2DWithTurrets
    {
        get { return grid2DWithTurrets; }
    }

    public Grid2D Grid2DNoTurrets
    {
        get { return grid2DNoTurrets; }
    }

    void Awake()
    {
        if (instance == null) instance = this;
        else
        {
            Debug.LogError("There is already an instance of " + this.GetType().ToString());
            return;
        }
        EventManager.AddListener<TurretCreatedEvent>(OnTurretCreatedEvent);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<TurretCreatedEvent>(OnTurretCreatedEvent);
    }

    public void ReadAndSpawnMap()
    {
        ReadMap();
        CreateGrid2D();
        SpawnMap();
    }

    void ReadMap()
    {
        // TODO go to file and read the map layout
        // For now, we use a hard-coded map
        map = new List<List<Tile>>()
        {
            new List<Tile>{ new TileWall(0,0),         new TileWall(0,1),     new TileWall(0,2),     new TileWall(0,3),     new TileWall(0,4) ,     new TileWall(0,5),     new TileWall(0,6),     new TileWall(0,7),     new TileWall(0,8)  },
            new List<Tile>{ new TileWall(1,0),         new TileFloor(1,1),    new TileFloor(1,2),    new TileWall(1,3),    new TileWall(1,4),     new TileWall(1,5),     new TileWall(1,6),     new TileWall(1,7),     new TileEnemySpawn(1,8)   },
            new List<Tile>{ new TileWall(2,0),         new TileFloor(2,1),    new TileWall(2,2),    new TileWall(2,3),    new TileFloor(2,4),     new TileFloor(2,5),     new TileWall(2,6),     new TileWall(2,7),     new TileFloor(2,8)   },
            new List<Tile>{ new TilePlayerBase(3,0),   new TileFloor(3,1),    new TileWall(3,2),    new TileFloor(3,3),    new TileWall(3,4),     new TileFloor(3,5),     new TileFloor(3,6),     new TileFloor(3,7),     new TileWall(3,8)   },
            new List<Tile>{ new TileFloor(4,0),         new TileFloor(4,1),     new TileFloor(4,2),     new TileFloor(4,3),     new TileFloor(4,4),     new TileWall(4,5),     new TileWall(4,6),     new TileWall(4,7),     new TileWall(4,8)   },
            new List<Tile>{ new TileWall(5,0),         new TileWall(5,1),     new TileWall(5,2),     new TileWall(5,3),     new TileWall(5,4),     new TileWall(5,5),     new TileWall(5,6),     new TileWall(5,7),     new TileWall(5,8)   },
        };
    }

    void CreateGrid2D()
    {
        grid2DWithTurrets = new Grid2D(map);
        grid2DNoTurrets = new Grid2D(map);
    }

    void SpawnMap()
    {
        // Spawn map based on the layout 
        UIMap uiMap = GUIManager.Instance.GetGUI(GUIType.MAP) as UIMap;
        if (uiMap == null)
        {
            Debug.LogError("No map GUI found when trying to spawn tiles");
            return;
        }
        uiMap.Setup(map.Count, map[0].Count);
        GameObject tilePrefab = SRResources.GUI.Map.TileElement.Load();
        tileControllers = new List<TileController>();
        foreach (List<Tile> tileRow in map)
        {
            foreach (Tile tile in tileRow)
            {
                GameObject tileInstance = Instantiate(tilePrefab, uiMap.tilesContainer);
                UITileElement uiTileElement = tileInstance.GetComponent<UITileElement>();
                uiTileElement.Init(tile);
                uiMap.AddTile(uiTileElement);
                TileController tileController = tileInstance.GetComponent<TileController>();
                tileController.Init(tile, uiTileElement);
                tileControllers.Add(tileController);
            }
        }
    }

    void OnTurretCreatedEvent(TurretCreatedEvent e)
    {
        grid2DWithTurrets.GetNodeFromPosition(e.tileColumn, e.tileRow).UpdateValues(0, false);
        SpawnerManager.Instance.RefreshPathfindingFromNewTurret(e.tileRow, e.tileColumn);
    }
}
