﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : Singleton<SpawnerManager>
{
    float timeBetweenEnemySpawnInSeconds, timeBetweenRoundsInSeconds;
    float enemySpawnElapsedTime, betweenRoundsElapsedTime;
    List<WaveData> waveDataList;
    int currentWaveDataIndex;
    int currentWaveSpawnedEnemies;
    GameObject enemyPrefab;
    List<TileController> spawnTiles;
    TileController playerBaseTile;
    int enemyIdCounter;
    bool areThereMoreWaves;
    List<Enemy> spawnedEnemiesList;

    public List<Enemy> SpawnedEnemiesList
    {
        get { return spawnedEnemiesList; }
    }

    void Awake()
    {
        if (instance == null) instance = this;
        else
        {
            Debug.LogError("There is already an instance of " + this.GetType().ToString());
            return;
        }
    }

    public void Init()
    {
        GameBalanceDefinition gameBalance = SRResources.GameBalance.General.GameBalance.Load();
        timeBetweenEnemySpawnInSeconds = gameBalance.timeBetweenEnemySpawnInSeconds;
        timeBetweenRoundsInSeconds = gameBalance.timeBetweenRoundsInSeconds;
        waveDataList = gameBalance.waves;
        enemySpawnElapsedTime = 0;
        betweenRoundsElapsedTime = 0;
        currentWaveDataIndex = 0;
        currentWaveSpawnedEnemies = 0;
        enemyPrefab = SRResources.GUI.Map.EnemyElement.Load();
        spawnTiles = MapManager.Instance.TileControllers.FindAll(x => x.MyTile is TileEnemySpawn);
        playerBaseTile = MapManager.Instance.TileControllers.Find(x => x.MyTile is TilePlayerBase);
        enemyIdCounter = 0;
        areThereMoreWaves = true;
        spawnedEnemiesList = new List<Enemy>();

        EventManager.AddListener<UpdateEvent>(OnUpdateEvent);
        EventManager.AddListener<EnemyDiedEvent>(OnEnemyDiedEvent);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<UpdateEvent>(OnUpdateEvent);
        EventManager.RemoveListener<EnemyDiedEvent>(OnEnemyDiedEvent);
    }

    void OnUpdateEvent(UpdateEvent e)
    {
        // count the update ticks and spawn when conditions are met
        enemySpawnElapsedTime += Time.deltaTime;
        betweenRoundsElapsedTime += Time.deltaTime;
        // Enemy spawn
        if (enemySpawnElapsedTime >= timeBetweenEnemySpawnInSeconds)
        {
            enemySpawnElapsedTime -= timeBetweenEnemySpawnInSeconds;
            SpawnNextEnemy();
        }
        // Wave finish
        if (betweenRoundsElapsedTime >= timeBetweenRoundsInSeconds)
        {
            betweenRoundsElapsedTime -= timeBetweenRoundsInSeconds;
            StartNextWave();
        }
    }

    void SpawnNextEnemy()
    {
        if (currentWaveDataIndex < waveDataList.Count)
        {
            WaveData wave = waveDataList[currentWaveDataIndex];
            if (currentWaveSpawnedEnemies < wave.enemyAmount)
            {
                SpawnEnemy(wave.enemyType);
            }
        }
    }

    void SpawnEnemy(EnemyType enemyType)
    {
        // Get spawn tile
        if (spawnTiles.Count == 0)
        {
            Debug.LogError("No spawn tiles available! Cannot spawn enemy");
            return;
        }
        Tile chosenSpawnTile = spawnTiles[UnityEngine.Random.Range(0, spawnTiles.Count)].MyTile;
        // Get enemy definition
        EnemyDefinition enemyDefinition = null;
        switch (enemyType)
        {
            case EnemyType.NORMAL:
                enemyDefinition = SRResources.GameBalance.EnemyDefinitions.EnemyNormalDefinition.Load();
                break;
            default:
                Debug.LogError("Unknown enemy type! Defaulting to normal");
                enemyDefinition = SRResources.GameBalance.EnemyDefinitions.EnemyNormalDefinition.Load();
                break;
        }
        // Get map container
        UIMap uiMap = GUIManager.Instance.GetGUI(GUIType.MAP) as UIMap;
        if (uiMap == null)
        {
            Debug.LogError("No map GUI found when trying to instantiate enemy");
            return;
        }
        Transform enemiesContainer = uiMap.enemiesContainer;
        // Instantiate and initialize
        GameObject enemyInstance = Instantiate(enemyPrefab, enemiesContainer);
        UIEnemy uiEnemy = enemyInstance.GetComponent<UIEnemy>();
        Enemy enemy = new Enemy(enemyIdCounter, enemyDefinition, chosenSpawnTile, playerBaseTile.MyTile);
        enemyIdCounter++;
        uiEnemy.Init(enemy);
        EnemyController enemyController = enemyInstance.GetComponent<EnemyController>();
        enemyController.Init(enemy);
        enemyController.SetPosition(chosenSpawnTile);
        spawnedEnemiesList.Add(enemy);
        EventManager.TriggerEvent(new EnemyMovedEvent(enemy.id));
        enemyController.ChangeState(StateMachine.StateType.MOVE);
        currentWaveSpawnedEnemies++;
    }

    void StartNextWave()
    {
        if (currentWaveDataIndex < waveDataList.Count)
        {
            currentWaveDataIndex++;
            currentWaveSpawnedEnemies = 0;
        }
        areThereMoreWaves = currentWaveDataIndex < waveDataList.Count - 1;
    }

    public bool AreThereMoreWaves()
    {
        return areThereMoreWaves;
    }

    void OnEnemyDiedEvent(EnemyDiedEvent e)
    {
        Enemy enemy = spawnedEnemiesList.Find(x => x.id == e.enemyId);
        spawnedEnemiesList.Remove(enemy);
        CheckForGameFinish();
    }
    
    public void RefreshPathfindingFromNewTurret(int newTurretRow, int newTurretColumn)
    {
        playerBaseTile = MapManager.Instance.TileControllers.Find(x => x.MyTile is TilePlayerBase);

        foreach (Enemy enemy in spawnedEnemiesList)
        {
            bool shouldRefreshPathfinding = enemy.IsTileInPath(newTurretRow, newTurretColumn);
            if (shouldRefreshPathfinding)
            {
                enemy.UpdatePathFinding(enemy.CurrentTile, playerBaseTile.MyTile, enemy.NextTile);
            }
        }
    }
    
    public void CheckForGameFinish()
    {
        if (!AreThereMoreWaves() && SpawnedEnemiesList.Count == 0)
        {
            EventManager.TriggerEvent(new GameWinEvent());
        }
    }
}
