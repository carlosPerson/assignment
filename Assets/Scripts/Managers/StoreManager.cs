﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreManager : Singleton<StoreManager>
{
    int currency;
    int currencyPerEnemyKilled;
    Dictionary<Tile.TileOptionType, TurretDefinition> turretDefinitionDictionary;

    public int Currency
    {
        get { return currency; }
        private set
        {
            currency = value;
            EventManager.TriggerEvent(new CurrencyChangedEvent());
        }
    }

    void Awake()
    {
        if (instance == null) instance = this;
        else
        {
            Debug.LogError("There is already an instance of " + this.GetType().ToString());
            return;
        }
    }

    public void Init()
    {
        turretDefinitionDictionary = new Dictionary<Tile.TileOptionType, TurretDefinition>()
        {
            { Tile.TileOptionType.TURRET,       SRResources.GameBalance.TurretDefinitions.Turret.Load() },
            { Tile.TileOptionType.FAST_TURRET,  SRResources.GameBalance.TurretDefinitions.TurretFast.Load() },
        };
        GameBalanceDefinition gameBalance = SRResources.GameBalance.General.GameBalance.Load();
        currency = gameBalance.initialCurrency;
        currencyPerEnemyKilled = gameBalance.currencyPerEnemyKilled;
        EventManager.AddListener<EnemyDiedEvent>(OnEnemyDiedEvent);
    }

    public TurretDefinition GetTurretDefinition(Tile.TileOptionType option)
    {
        return turretDefinitionDictionary[option];
    }

    void OnEnemyDiedEvent(EnemyDiedEvent e)
    {
        Currency += currencyPerEnemyKilled;
    }

    public bool CanPurchaseOption(Tile.TileOptionType tileOption)
    {
        return currency >= GetTurretDefinition(tileOption).price;
    }

    public bool PurchaseOption(Tile.TileOptionType tileOption)
    {
        bool result = false;
        if (CanPurchaseOption(tileOption))
        {
            Currency -= GetTurretDefinition(tileOption).price;
            result = true;
        }
        else result = false;
        return result;
    }
}
