﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : Singleton<CameraManager>
{
    [HideInInspector]
    public CameraController cameraController;

    public void Init()
    {
        cameraController = SRResources.Prefabs.MapCamera.Instantiate().GetComponent<CameraController>();
    }
}
