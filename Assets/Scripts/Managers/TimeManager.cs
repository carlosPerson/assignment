﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : Singleton<TimeManager>
{
    bool isOn;
    UpdateEvent cachedUpdateEvent;

    void Awake()
    {
        if (instance == null) instance = this;
        else
        {
            Debug.LogError("There is already an instance of " + this.GetType().ToString());
            return;
        }
        cachedUpdateEvent = new UpdateEvent();
        EventManager.AddListener<GameWinEvent>(OnGameWinEvent);
        EventManager.AddListener<GameOverEvent>(OnGameOverEvent);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<GameWinEvent>(OnGameWinEvent);
        EventManager.RemoveListener<GameOverEvent>(OnGameOverEvent);
    }

    public void Resume()
    {
        if (cachedUpdateEvent == null) cachedUpdateEvent = new UpdateEvent();
        isOn = true;
    }

    public void Pause()
    {
        isOn = false;
    }

    void Update()
    {
        if (isOn) EventManager.TriggerEvent(cachedUpdateEvent);
    }

    void OnGameOverEvent(GameOverEvent e)
    {
        Pause();
    }

    void OnGameWinEvent(GameWinEvent e)
    {
        Pause();
    }
}
