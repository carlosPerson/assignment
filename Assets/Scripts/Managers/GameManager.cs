﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    void Awake()
    {
        if (instance == null) instance = this;
        else
        {
            Debug.LogError("There is already an instance of " + this.GetType().ToString());
            return;
        }
        EventManager.AddListener<GameOverEvent>(OnGameOverEvent);
        EventManager.AddListener<GameWinEvent>(OnGameWinEvent);
        // Load basic modules
        SRResources.Prefabs.EventSystem.Instantiate();
        CameraManager.Instance.Init();
        GUIManager.instance.Init();
        // Show initial GUI
        GUIManager.Instance.ShowGUI(GUIType.MAIN_MENU);
    }

    void OnDestroy()
    {
        EventManager.RemoveListener<GameOverEvent>(OnGameOverEvent);
        EventManager.RemoveListener<GameWinEvent>(OnGameWinEvent);
    }

    public void StartGame()
    {
        GUIManager.Instance.CloseGUI(GUIType.MAIN_MENU);
        GUIManager.Instance.ShowGUI(GUIType.MAP);
        StoreManager.Instance.Init();
        GUIManager.Instance.ShowGUI(GUIType.HUD);
        MapManager.Instance.ReadAndSpawnMap();
        SpawnerManager.Instance.Init();
        TimeManager.Instance.Resume();
    }

    void OnGameOverEvent(GameOverEvent e)
    {
        GUIManager.Instance.ShowGUI(GUIType.GAME_OVER);
    }

    void OnGameWinEvent(GameWinEvent e)
    {
        GUIManager.Instance.ShowGUI(GUIType.VICTORY);
    }
}
