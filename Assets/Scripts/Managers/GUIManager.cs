﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager : Singleton<GUIManager>
{
    Dictionary<GUIType, GameObject> guiPrefabs;
    Dictionary<GUIType, UIScreen> currentlyOpenGUIs;
    GameObject baseCanvas;

    void Awake()
    {
        if (instance == null) instance = this;
        else
        {
            Debug.LogError("There is already an instance of " + this.GetType().ToString());
            return;
        }
    }

    public void Init()
    {
        InitGUIDictionary();
    }

    void InitGUIDictionary()
    {
        baseCanvas = SRResources.GUI.BaseCanvas.Load();
        guiPrefabs = new Dictionary<GUIType, GameObject>()
        {
            { GUIType.MAP,          SRResources.GUI.Map.GUI_Map },
            { GUIType.HUD,          SRResources.GUI.HUD.GUI_HUD },
            { GUIType.MAIN_MENU,    SRResources.GUI.MainMenu.GUI_MainMenu },
            { GUIType.GAME_OVER,    SRResources.GUI.GameOver.GUI_GameOver },
            { GUIType.VICTORY,      SRResources.GUI.Victory.GUI_Victory },
        };
        currentlyOpenGUIs = new Dictionary<GUIType, UIScreen>();
    }

    public UIScreen ShowGUI(GUIType guiType)
    {
        UIScreen uiScreen = null;
        if (currentlyOpenGUIs.ContainsKey(guiType))
        {
            uiScreen = currentlyOpenGUIs[guiType];
            Debug.LogWarning("GUI + " + guiType + " was already open");
        }
        else
        {
            Canvas parentCanvas = Instantiate(baseCanvas).GetComponent<Canvas>();
            parentCanvas.worldCamera = CameraManager.Instance.cameraController.myCamera;
            uiScreen = Instantiate(guiPrefabs[guiType], parentCanvas.transform).GetComponent<UIScreen>();
            uiScreen.parentCanvas = parentCanvas;
            uiScreen.parentCanvas.sortingOrder = uiScreen.sortingOrder;
            currentlyOpenGUIs.Add(guiType, uiScreen);
        }
        return uiScreen;
    }

    public UIScreen GetGUI(GUIType guiType)
    {
        UIScreen uiScreen = null;
        if (currentlyOpenGUIs.ContainsKey(guiType))
        {
            uiScreen = currentlyOpenGUIs[guiType];
        }
        return uiScreen;
    }

    public void CloseGUI(GUIType guiType)
    {
        if (currentlyOpenGUIs.ContainsKey(guiType))
        {
            UIScreen uiScreen = currentlyOpenGUIs[guiType];
            uiScreen.Close();
            Destroy(uiScreen.parentCanvas.gameObject);
            currentlyOpenGUIs.Remove(guiType);
        }
    }
}
