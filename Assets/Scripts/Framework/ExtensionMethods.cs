﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static bool Approximately(this Quaternion q1, Quaternion q2, float epsilon = 0.1f)
    {
        return Mathf.Abs(Quaternion.Dot(q1, q2)) > 1 - epsilon;
    }
}
