﻿using UnityEngine;

public abstract class GameEvent
{
}

public abstract class EnemyEvent : GameEvent
{
    public int enemyId;
    public EnemyEvent(int enemyId)
    {
        this.enemyId = enemyId;
    }
}

public abstract class TileEvent : GameEvent
{
    public int tileRow, tileColumn;
    public TileEvent(int tileRow, int tileColumn)
    {
        this.tileRow = tileRow;
        this.tileColumn = tileColumn;
    }
}

public class UpdateEvent : GameEvent { }

public class GameWinEvent : GameEvent { }

public class GameOverEvent : GameEvent { }

public class CurrencyChangedEvent : GameEvent { }

public class TileOptionPurchasedEvent : TileEvent
{
    public Tile.TileOptionType optionType;

    public TileOptionPurchasedEvent(int tileRow, int tileColumn, Tile.TileOptionType optionType) : base(tileRow, tileColumn)
    {
        this.optionType = optionType;
    }
}

#region enemy events

public class EnemyMovedEvent : EnemyEvent
{
    public EnemyMovedEvent(int enemyId) : base(enemyId)
    {
    }
}

public class EnemyReceivedDamageEvent : EnemyEvent
{
    public EnemyReceivedDamageEvent(int enemyId) : base(enemyId)
    {
    }
}

public class EnemyDiedEvent : EnemyEvent
{
    public EnemyDiedEvent(int enemyId) : base(enemyId)
    {
    }
}

#endregion


#region turret events

public class TurretCreatedEvent : TileEvent
{
    public TurretCreatedEvent(int tileRow, int tileColumn) : base(tileRow, tileColumn)
    {
    }
}

public class TurretRotatedEvent : TileEvent
{
    public Quaternion targetRotation;
    public TurretRotatedEvent(int tileRow, int tileColumn, Quaternion targetRotation) : base(tileRow, tileColumn)
    {
        this.targetRotation = targetRotation;
    }
}

public class TurretDestroyedEvent : TileEvent
{
    public TurretDestroyedEvent(int tileRow, int tileColumn) : base(tileRow, tileColumn)
    {
    }
}

public class TurretShotEvent : TileEvent
{
    public TurretShotEvent(int tileRow, int tileColumn) : base(tileRow, tileColumn)
    {
    }
}

#endregion

#region UI events

public class OpeningTileContextMenuEvent : GameEvent { }

public class TilesBlockingButtonClickedEvent : GameEvent { }

#endregion