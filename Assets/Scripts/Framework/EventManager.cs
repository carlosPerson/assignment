﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EventManager : Singleton<EventManager>
{
    //private static EventManager s_Instance = null;

    public delegate void EventDelegate<T>(T e) where T : GameEvent;

    private delegate void EventDelegate(GameEvent e);

    private Dictionary<System.Type, EventDelegate> delegates = new Dictionary<System.Type, EventDelegate>();
    private Dictionary<System.Delegate, EventDelegate> delegateLookup = new Dictionary<System.Delegate, EventDelegate>();
    private Dictionary<System.Delegate, System.Delegate> onceLookups = new Dictionary<System.Delegate, System.Delegate>();

    //void Awake()
    //{
    //    if (instance == null) instance = this;
    //    else
    //    {
    //        Debug.LogError("There is already an instance of " + this.GetType().ToString());
    //        return;
    //    }
    //}

    private EventDelegate AddDelegate<T>(EventDelegate<T> del) where T : GameEvent
    {
        // Early-out if we've already registered this delegate
        if (Instance.delegateLookup.ContainsKey(del))
            return null;

        // Create a new non-generic delegate which calls our generic one.
        // This is the delegate we actually invoke.
        EventDelegate internalDelegate = (e) => del((T)e);
        Instance.delegateLookup[del] = internalDelegate;

        EventDelegate tempDel;
        if (Instance.delegates.TryGetValue(typeof(T), out tempDel))
        {
            Instance.delegates[typeof(T)] = tempDel += internalDelegate;
        }
        else
        {
            Instance.delegates[typeof(T)] = internalDelegate;
        }

        return internalDelegate;
    }

    public static void AddListener<T>(EventDelegate<T> del) where T : GameEvent
    {
        if (Instance != null)
        {
            Instance.AddDelegate<T>(del);
        }
    }

    public static void AddListenerOnce<T>(EventDelegate<T> del) where T : GameEvent
    {
        if (Instance != null)
        {
            EventDelegate result = Instance.AddDelegate<T>(del);

            if (result != null)
            {
                // remember this is only called once
                Instance.onceLookups[result] = del;
            }
        }
    }

    public static void RemoveListener<T>(EventDelegate<T> del) where T : GameEvent
    {
        if (instance != null)
        {
            EventDelegate internalDelegate;
            if (Instance.delegateLookup.TryGetValue(del, out internalDelegate))
            {
                EventDelegate tempDel;
                if (Instance.delegates.TryGetValue(typeof(T), out tempDel))
                {
                    tempDel -= internalDelegate;
                    if (tempDel == null)
                    {
                        Instance.delegates.Remove(typeof(T));
                    }
                    else
                    {
                        Instance.delegates[typeof(T)] = tempDel;
                    }
                }

                Instance.delegateLookup.Remove(del);
            }
        }
    }

    public static void RemoveAll()
    {
        if (Instance != null)
        {
            Instance.delegates.Clear();
            Instance.delegateLookup.Clear();
            Instance.onceLookups.Clear();
        }
    }

    public static bool HasListener<T>(EventDelegate<T> del) where T : GameEvent
    {
        if (Instance != null)
        {
            return Instance.delegateLookup.ContainsKey(del);
        }
        else
            return false;
    }

    public static void TriggerEvent(GameEvent e)
    {
        if (Instance != null)
        {
            EventDelegate del;
            if (Instance.delegates.TryGetValue(e.GetType(), out del))
            {
                del.Invoke(e);

                // remove listeners which should only be called once
                // check again for delegates (previous trigger could have unsubscribed stuff)
                if (Instance.delegates.TryGetValue(e.GetType(), out del))
                {
                    foreach (EventDelegate k in Instance.delegates[e.GetType()].GetInvocationList())
                    {
                        if (Instance.onceLookups.ContainsKey(k))
                        {
                            Instance.delegates[e.GetType()] -= k;

                            if (Instance.delegates[e.GetType()] == null)
                            {
                                Instance.delegates.Remove(e.GetType());
                            }

                            Instance.delegateLookup.Remove(Instance.onceLookups[k]);
                            Instance.onceLookups.Remove(k);
                        }
                    }
                }
            }
        }
        else
        {
            Debug.LogWarning("Event Manager has not been instanciated");
        }
    }

    public void OnDestroy()
    {
        RemoveAll();
    }

    public void OnApplicationQuit()
    {
        RemoveAll();
    }
}