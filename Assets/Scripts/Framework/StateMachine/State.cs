﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State
{
    protected Enemy enemy;

    public State(Enemy enemy)
    {
        this.enemy = enemy;
    }

    public virtual void Enter()
    {
        Debug.Log("OnEnter: " + this.GetType().ToString());
    }

    public virtual void Execute()
    {
        //Debug.Log("OnExecute: " + this.GetType().ToString());
    }

    public virtual void Exit()
    {
        Debug.Log("OnExit: " + this.GetType().ToString());
    }
}