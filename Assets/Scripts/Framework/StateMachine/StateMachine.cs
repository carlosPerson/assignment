﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine
{
    State currentState;

    StateIdle stateIdle;
    StateMove stateMove;

    public State CurrentState
    {
        get { return currentState; }
    }

    public enum StateType { IDLE, MOVE }

    public StateMachine(Enemy enemy)
    {
        stateIdle = new StateIdle(enemy);
        stateMove = new StateMove(enemy);
        ChangeState(StateType.IDLE); // initial state
    }

    public void ChangeState(StateType newStateType)
    {
        if (currentState != null) currentState.Exit();
        State newState = null;
        switch (newStateType)
        {
            case StateType.IDLE:
                newState = stateIdle;
                break;
            case StateType.MOVE:
                newState = stateMove;
                break;
            default:
                Debug.LogError("Unknown state type, defaulting to idle");
                newState = stateIdle;
                break;
        }
        currentState = newState;
        currentState.Enter();
    }

    public void Update()
    {
        if (currentState != null) currentState.Execute();
    }
}